import express from 'express';
import timer from 'contimer';
import transit from 'transit-immutable-js';

import Logger from './logger';

const logger = Logger('lib/express-extensions');
const skipRe = /\.(gif|jpe?g|png|svg|css|js|txt|woff2?|ttf|otf|eot|ico|js\.map|(js|css)\?v=[a-bA-Z0-9]*)$/i;

express.request.getState = function getState() {
    return this.state;
};

const originalResponseEnd = express.response.end;
express.response.end = function ExpressResponseCustomEnd(...args) {
    const { req } = this;
    const totalTimer = timer.stop(req, 'total');
    const requestProcessTime = (totalTimer && totalTimer.time) || 'unknown';

    if (!skipRe.test(req.originalUrl)) {
        logger.info(
            [
                `request processing time: [${requestProcessTime} ms]`,
                `url: ${req.originalUrl}`,
            ].join(', ')
        );
    }

    return originalResponseEnd.apply(this, args);
};

express.response.transitJSON = function ExpressResponseTransitJSON(...args) {
    this.type('application/json');
    return this.send(transit.toJSON(...args));
};
