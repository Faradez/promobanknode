import request from 'request';
import prettyjson from 'prettyjson';

import compact from 'lodash/compact';
import isArray from 'lodash/isArray';
import isString from 'lodash/isString';
import isPlainObject from 'lodash/isPlainObject';
import isFinite from 'lodash/isFinite';
import isEmpty from 'lodash/isEmpty';
import get from 'lodash/get';

import md5 from 'md5';
import chalk from 'chalk';

import config from '../lib/config';
import Logger from '../lib/logger';
import redis from '../lib/redis/apiRedis';
import {
    getElapsedTime,
    getStartTime,
} from '../lib/helpers';

const logger = Logger('providers/baseProvider');

const colors = new chalk.constructor({
    enabled: config.IS_ENV_DEVELOPMENT,
});

const getUniqId = (() => {
    let counter = 0;

    return () => {
        counter += 1;

        if (!Number.isSafeInteger(counter)) {
            counter = 0;
        }

        return `${process.pid}_${counter}`;
    };
})();

class RequestComposer {
    cacheKey = null;
    meta = null;
    method = null;
    requestId = null;
    methodParams = null;
    cacheExpire = null;
    headers = {};
    token = null;

    static allowedFields = [
        'asCurl',
        'getCacheKey',
        'getRequestOptions',
    ];

    static defaultHeaders = {
        'X-API-KEY': config.apiKey,
        'X-Forwarded-Proto': config.IS_HTTPS ? 'https' : 'http',
        'Content-Type': 'application/json',
    };

    constructor({
        method,
        methodParams = {},
        expire = null,
        token = null,
        headers = {},
    }) {
        this.setMethod(method);
        this.setMethodParams(methodParams);
        this.mergeHeaders(headers);
        this.setToken(token);
        this.setExpire(expire);

        return new Proxy(this, {
            get(target, name) {
                if (RequestComposer.allowedFields.includes(name)) {
                    return target[name].bind(target);
                }

                return undefined;
            },
        });
    }

    setMethod(method) {
        if (!method || !isString(method)) {
            throw new Error('RequestComposer(...): no method in param');
        }

        this.cacheKey = null;
        this.meta = null;
        this.method = method;
        this.requestId = getUniqId();
        this.setMethodParams({});
        this.setExpire(null);
    }

    setMethodParams(methodParams) {
        if (
            methodParams &&
            !(isPlainObject(methodParams) || isArray(methodParams))
        ) {
            throw new Error('RequestComposer(...): wrong methodParams param');
        }

        this.cacheKey = null;
        this.methodParams = methodParams;
    }

    mergeHeaders(headers) {
        if (headers && !isPlainObject(headers)) {
            throw new Error('RequestComposer(...): wrong headers param');
        }

        this.headers = { ...this.headers, ...headers };
        this.setExpire(null);
    }

    setToken(token) {
        if (token && !isString(token)) {
            throw new Error('RequestComposer(...): wrong token param');
        }

        this.token = token;
    }

    setExpire(expire) {
        const expireVal =
            expire || null;

        if (expireVal && (!isFinite(expireVal) || expireVal < 0)) {
            throw new Error('RequestComposer(...): wrong expireVal param');
        }

        if (expireVal && !this.headers) {
            throw new Error(
                'RequestComposer(...): wrong expireVal param with headers'
            );
        }

        this.cacheExpire = expireVal;
    }

    getCacheKey() {
        if (!this.cacheKey) {
            this.cacheKey = `${this.method}-${md5(
                JSON.stringify(this.methodParams)
            )}-${this.token || ''}`;
        }

        return this.cacheKey;
    }

    getUrl() {
        const reqOpts = {};
        let { apiVersion } = reqOpts;

        if (typeof apiVersion === 'undefined') {
            apiVersion = 'v0';
        }

        if (typeof apiVersion !== 'string') {
            throw new Error(
                `Api version must be string for method ${this.method}.`
            );
        }

        if (!config.apiUrl[apiVersion]) {
            throw new Error(
                `Api version "${apiVersion}" not found for method ${this
                    .method}.`
            );
        }

        return config.apiUrl[apiVersion];
    }

    getRequestOptions() {
        const rawBody = {
            jsonrpc: '2.0',
            method: this.method,
            id: this.requestId,
        };

        if (!isEmpty(this.methodParams)) {
            rawBody.params = this.methodParams;
        }

        const uri = this.getUrl();

        const options = {
            uri,
            timeout: 20000,
            method: 'POST',
            headers: {
                ...RequestComposer.defaultHeaders,
                ...this.headers,
            },
            body: JSON.stringify(rawBody),
            requestId: this.requestId,
            rawBody,
            cacheExpire: this.cacheExpire,
        };

        if (this.token) {
            options.headers = { ...options.headers, Cookie: this.token };
        }

        return options;
    }

    asCurl() {
        const options = this.getRequestOptions();

        const headers = Object.keys(options.headers).map(
            key => `-H '${key}: ${options.headers[key]}'`
        );

        return [
            'curl -i',
            '-X POST',
            headers.join(' '),
            `--data-binary '${options.body}'`,
            `'${options.uri}'`,
        ].join(' ');
    }
}

function getTimeColor(time) {
    if (time > 200) {
        return colors.red;
    }

    if (time > 100) {
        return colors.yellow;
    }

    return colors.green;
}

function logRequestReadFromRedis({ req, elapsedTime }) {
    const { method } = req.getRequestOptions().rawBody;
    const timeColor = getTimeColor(elapsedTime);

    logger.debug(
        compact([
            `${colors.yellow(method)}`,
            `[${timeColor(elapsedTime)} ms]`,
            `${colors.yellow('read from redis')}`,
            `""" ${req.asCurl()} """`,
        ]).join(', ')
    );
}

function logRequest({
    req, elapsedTime, statusCode, err,
}) {
    const { method } = req.getRequestOptions().rawBody;
    const timeColor = getTimeColor(elapsedTime);
    const errMsg = err && err.message ? `error: ${err.message}` : null;

    const logMsg = compact([
        `${statusCode === 200 ? colors.yellow(method) : colors.red(method)}`,
        `[${timeColor(elapsedTime)} ms]`,
        statusCode === 200
            ? `code ${statusCode}`
            : `code ${colors.red(statusCode)}`,
        `""" ${req.asCurl()} """`,
        errMsg,
    ]).join(', ');

    if (errMsg) {
        logger.error(logMsg);
    } else {
        logger.info(logMsg);
    }
}

function logPrettyResponse({ req, ansBody }) {
    try {
        const data = get(JSON.parse(ansBody), req.getMeta().logPrettyPath);
        logger.debug(
            [
                colors.bold.inverse('<<BEGIN'),
                prettyjson.render(data),
                colors.bold.inverse('<<END'),
            ].join('\n')
        );
    } catch (e) {
        logger.debug(
            colors.red('could not pretty print response body, data was:'),
            ansBody
        );
    }
}

function toRedisPacket(data, timestampSet, timestampExpire) {
    return JSON.stringify({
        d: data,
        ts: timestampSet,
        te: timestampExpire,
    });
}

function fromRedisPacket(packet) {
    const parsed = JSON.parse(packet);

    return {
        data: parsed.d,
        timestampSet: parsed.ts,
        timestampExpire: parsed.te,
    };
}

function requestUrlFromServer(req) {
    const startTime = getStartTime();

    return new Promise((resolve, reject) => {
        const options = req.getRequestOptions();
        const { cacheExpire } = options;
        request(options, (requestErr, res, ansBody) => {
            const elapsedTime = getElapsedTime(startTime);

            const ans = {
                err: requestErr,
                statusCode: res && res.statusCode,
                headers: res && res.headers,
            };

            const err = {};

            if (requestErr) {
                err.message = `${requestErr.code}`;
                err.statusCode = 500;

                logRequest({
                    req,
                    elapsedTime,
                    statusCode: res && res.statusCode,
                    err,
                    ans,
                });

                reject(err);
                return;
            }

            const reqMeta = req.getMeta();

            if (reqMeta.logPrettyResponse) {
                logPrettyResponse({ req, ansBody });
            }

            if ([200, 201, 204, 304].indexOf(res.statusCode) === -1) {
                err.message = `invalid status code ${res.statusCode}`;
                err.statusCode = res.statusCode;

                logRequest({
                    req,
                    elapsedTime,
                    statusCode: res && res.statusCode,
                    err,
                });

                reject(err);
                return;
            }

            let body = { id: null };

            try {
                body = JSON.parse(ansBody);
            } catch (exx) {
                body = {};
            }

            if (body && body.id !== options.requestId) {
                err.message = `requestId=${options.requestId} and responseId=${body.id} mismatch`;
                err.body = body;
                err.statusCode = 500;
            } else if (body) {
                err.body = body;

                if (body.error) {
                    const methodNotFound = -32601;
                    err.statusCode =
                        body.error.code === methodNotFound ? 404 : 500;
                    err.message = `${JSON.stringify(body.error)}`;
                } else if (body.errors) {
                    err.statusCode = 400;
                    err.message = `${JSON.stringify(body.errors)}`;
                } else if (
                    body.result &&
                    body.result.errors &&
                    !reqMeta.errorsInBody
                ) {
                    err.statusCode = 400;
                    err.message = body.result.errors;
                }
            }

            ans.body = body.result;

            logRequest({
                req,
                elapsedTime,
                statusCode: res && res.statusCode,
                err,
            });

            if (err && err.message) {
                reject(err);
                return;
            }

            resolve(ans);

            if (!cacheExpire) {
                return;
            }

            const timestampSet = Date.now();

            // prettier-ignore
            const timestampExpire = timestampSet + (cacheExpire * 1000);

            const packet = toRedisPacket(
                body.result,
                timestampSet,
                timestampExpire
            );

            redis.set(req.getCacheKey(), packet, cacheExpire);
        });
    });
}

function requestRedis(req) {
    const startTime = getStartTime();

    return redis.get(req.getCacheKey()).then((packet) => {
        const elapsedTime = getElapsedTime(startTime);
        const { data, timestampSet, timestampExpire } = fromRedisPacket(packet);

        logRequestReadFromRedis({
            req,
            elapsedTime,
        });

        const timeLeft = timestampExpire - Date.now();
        const timeSpan = timestampExpire - timestampSet;

        if (timeLeft <= config.redisApiPrefetchThreshold * timeSpan) {
            requestUrlFromServer(req);
        }

        return {
            body: data,
            statusCode: 200,
        };
    });
}

export default function askResource(
    method,
    methodParams = {},
    token,
    headers,
    customRequestParams = {}
) {
    let req;

    try {
        req = new RequestComposer({
            method,
            methodParams,
            expire: customRequestParams.expire,
            token,
            headers,
        });
    } catch (err) {
        throw err;
    }

    const { forceCacheRefresh } = customRequestParams;

    if (
        typeof forceCacheRefresh !== 'undefined' &&
        typeof forceCacheRefresh !== 'boolean'
    ) {
        throw Error('forceCacheRefresh must be boolean or undefined');
    }

    if (forceCacheRefresh || !req.getRequestOptions().cacheExpire) {
        return requestUrlFromServer(req);
    }

    return requestRedis(req).catch(() => requestUrlFromServer(req));
}
