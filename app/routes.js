/* eslint sort-keys: ["error", "asc", {natural: true}] */

export default {
    api: {},

    external: {},

    pages: {
        about: '/about_project',
        offers: '/offers',
        people: '/people',
        register: '/registration/employer',
    },
};
