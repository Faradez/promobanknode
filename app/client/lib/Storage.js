/* global localStorage, SecurityError */
import transit from 'transit-immutable-js';

/* eslint-disable class-methods-use-this */
class NoopStorage {
    get() {
        return null;
    }

    set() {}

    remove() {}
}
/* eslint-enable class-methods-use-this */

export default class Storage {
    constructor(suffix = '') {
        try {
            if (typeof localStorage === 'undefined') {
                return new NoopStorage();
            }
        } catch (e) {
            if (
                (typeof SecurityError !== 'undefined' &&
                    e instanceof SecurityError) ||
                e.name === 'SecurityError'
            ) {
                return new NoopStorage();
            }

            throw e;
        }

        this.suffix = suffix;
        return this;
    }

    set(key, value) {
        try {
            localStorage.setItem(
                key + this.suffix,
                transit.toJSON(value)
            );
        } catch (err) {
            console.error('localStorage catch error by setItem', err);
        }
    }

    get(key) {
        try {
            const data = localStorage.getItem(key + this.suffix);
            return transit.fromJSON(data);
        } catch (err) {
            console.error('localStorage catch error by getItem', err);
            return null;
        }
    }

    remove(key) {
        localStorage.removeItem(key + this.suffix);
    }
}
