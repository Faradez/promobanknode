import PropTypes from 'prop-types';
import { PureComponent } from 'react';

const promises = {};

export default class InjectScriptTag extends PureComponent {
    static propTypes = {
        onLoad: PropTypes.func,
        url: PropTypes.string.isRequired,
    };

    componentDidMount() {
        this.processScripts();
    }

    componentDidUpdate() {
        this.processScripts();
    }

    processScripts() {
        const { url, onLoad } = this.props;

        const promise =
            promises[url] ||
            new Promise((resolve) => {
                const script = document.createElement('script');
                script.src = this.props.url;
                script.async = true;
                script.onload = resolve;
                document.body.appendChild(script);
            });

        promises[url] = promise;

        if (typeof onLoad === 'function') {
            onLoad(promise);
        }
    }

    render() {
        return null;
    }
}
