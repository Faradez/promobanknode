import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Page from './Page';

import Header from './components/Header';

class HelloWord extends React.Component {     
    render() {
        return null;
    }
}

const PageRenderer = rule => props => (
    <Page
        {...props}
        main={<rule.parts.main />}
        header={rule.parts.header ? <rule.parts.header /> : null}
        footer={rule.parts.footer ? <rule.parts.footer /> : null}
        {...rule.props}
    />
);

const rules = [
    {
        path: '/',
        parts: {                                        
            main: HelloWord,  
            header: Header,
            // footer: Footer,
        },
        props: {
            isHeaderNavigationHidden: true,
            pageClassName: 'pageBgWhite',
        },
    },
];

export default (
    <Switch>
        {rules.map((rule, index) => (
            <Route
                // eslint-disable-next-line react/no-array-index-key
                key={index}
                exact={rule.exact}
                path={rule.path}
                render={PageRenderer(rule)}
            />
        ))}
    </Switch>
);
