const find = require('lodash/find');
const os = require('os');

function getExternalAddress() {
    const interfaces = os.networkInterfaces();

    let alias;

    Object.keys(interfaces).forEach((devName) => {
        if (alias) return;

        const addrs = interfaces[devName];

        alias = find(
            addrs,
            addr =>
                addr.family === 'IPv4' &&
                addr.address !== '127.0.0.1' &&
                !addr.internal
        );
    });

    return alias ? alias.address : '0.0.0.0';
}

module.exports = getExternalAddress;
