import Logger from '../lib/logger';

const log = Logger('lib/errorsLogger');

export default function registerErrorHandler() {
    process.on('uncaughtException', (err) => {
        log.error(err);
    });

    process.on('unhandledRejection', (reason) => {
        log.error('unhandledRejection', reason);
    });
}
