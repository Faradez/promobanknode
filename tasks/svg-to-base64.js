const gulp = require('gulp');
const postcss = require('gulp-postcss');
const base64 = require('postcss-base64');

gulp.task('svg-to-base64', () =>
    gulp
        .src(['./dist/app.css', './dist/icons-set.css'])
        .pipe(
            postcss([
                base64({
                    extensions: ['.svg(?!\\?ignore=1)'],
                    pattern: /data:image/i,
                    prepend: 'data:image/svg+xml;',
                }),
            ])
        )
        .pipe(gulp.dest('./dist/'))
);
