import md5 from 'md5';
import Helmet from 'react-helmet';
import fs from 'fs';
import each from 'lodash/each';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { fromJS } from 'immutable';
import transit from 'transit-immutable-js';

import renderRedis from './redis/renderRedis';
import config from './config';
import clientConfig from '../client/config';

import layouts from '../layouts';

import Routes from '../client/Routes';

const reducers = state => state;
const storeEnhancer = applyMiddleware(thunk);

const versions = Object.assign(
    {
        'app.css': '',
        'app.js': '',
        'vendor.js': '',
    },
    JSON.parse(fs.readFileSync('./assets.json', 'utf8'))
);

each(versions, (obj, name) => {
    each(obj, (src, ext) => {
        versions[`${name}.${ext}`] = src;
    });
});

export function renderLayout(
    data = {},
    layoutName = 'default'
) {
    return layouts[layoutName](data);
}

function getOptionsCacheKey(state) {
    const hash = md5(transit.toJSON(state));
    return `layout-${state.location}-${hash}-${config.buildNumber}`;
}

export function cacheRenderOptions(state, renderOptions) {
    const key = getOptionsCacheKey(state);
    const entry = transit.toJSON(renderOptions);
    return renderRedis.set(key, entry, 30000);
}

export function getRenderOptionsFromCache(state) {
    const key = getOptionsCacheKey(state);
    return renderRedis
        .get(key)
        .then(entry => ({ ...transit.fromJSON(entry), retrievedFromCache: true }));
}

export function getRenderOptions(state) {
    const store = createStore(reducers, fromJS(state), storeEnhancer);
    const initialState = store.getState();

    const staticContext = {};
    const component = React.createElement(
        StaticRouter,
        { location: state.location, context: staticContext },
        Routes
    );

    const reactString = renderToString(
        React.createElement(Provider, { store }, component)
    );
    const helmet = Helmet.renderStatic();

    return {
        content: reactString,
        clientConfig: encodeURIComponent(JSON.stringify(clientConfig)),
        state: encodeURIComponent(
            transit.toJSON(initialState)
        ),
        helmetTitle: helmet.title.toString(),
        helmetMeta: helmet.meta.toString(),
        helmetLink: helmet.link.toString(),
        ...versions,
        staticContext,
    };
}
