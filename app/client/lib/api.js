import axios from 'axios';
import qs from 'qs';
import transit from 'transit-immutable-js';

const axiosInstance = axios.create({
    timeout: 20000, // 20 seconds
    headers: {
        'X-Requested-With': 'xmlhttprequest',
        post: {},
    },
});

export function setCSRF(token) {
    axiosInstance.defaults.headers.post['X-CSRF-Token'] = token;
}

function getAxiosURL(url, params) {
    const query = (/\?/.test(url) ? '&' : '?') + qs.stringify(params);
    return `${url}${query}`;
}

export function get(url, params) {
    const URL = getAxiosURL(url, params);
    return axiosInstance.get(URL).then(ans => ans.data);
}

export function transitGet(url, params) {
    const URL = getAxiosURL(url, params);
    return axiosInstance
        .get(URL, {
            transformResponse: [
                function transitFromJSON(data) {
                    return transit.fromJSON(data);
                },
            ],
        })
        .then(ans => ans.data);
}

export function post(url, params) {
    return axiosInstance.post(url, params);
}
