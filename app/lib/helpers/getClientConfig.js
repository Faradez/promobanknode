const pick = require('lodash/pick');

function getClientConfig(serverConfig) {
    const config = Object.assign(
        {},
        pick(serverConfig, [
            'serverName',
            'serverPort',
            'IS_HTTPS',
        ]),
        {
            env: process.env.NODE_ENV || 'development',
        }
    );

    const buildNumber = parseInt(process.env.BUILD_NUMBER, 10);

    config.buildNumber = Number.isNaN(buildNumber)
        ? new Date().toJSON()
        : JSON.stringify(buildNumber);

    config.isStable = Boolean(serverConfig.isStable);

    return config;
}

module.exports = getClientConfig;
