import React, { Component, Fragment } from 'react';
import cx from 'classnames';
import styles from './Header.css';
import routes from '../../../routes';

export default class Header extends Component {
    constructor() {
        super();

        this.state = {
            isMobMenuOpen: false,
        };
    }
    onClickMobMenu = () => {
        this.setState(prevState => ({
            isMobMenuOpen: !prevState.isMobMenuOpen,
        }));
    };

    componentDidMount() {
        this.handlerMouseMove = (e) => {
            const { target } = e;
            if (!target) {
                return;
            }

            if (!target.getAttribute) {
                return;
            }
            if (
                target.dataset.target === 'subnav' ||
                target.dataset.target === 'linkNav'
            ) {
                return;
            }

            this.setState({
                activeSubnav: null,
            });
        };
        document.addEventListener('mousemove', this.handlerMouseMove);
    }

    componentWillUnmount() {
        document.removeEventListener('mousemove', this.handlerMouseMove);
    }

    onMouseEnter = (e) => {
        const activeSubnav = e.currentTarget.dataset.navId;
        this.setState({
            activeSubnav,
        });
    };

    renderNonAuth() {
        const { isMobMenuOpen } = this.state;

        return (
            <div className={styles.content}>
                <div className={cx(styles.contentChild, styles.left)}>
                    {isMobMenuOpen ? (
                        <Fragment>
                            <button
                                className={cx(
                                    styles.btnMenuItem,
                                    styles.register
                                )}
                            >
                                Регистрация
                            </button>
                            <button
                                className={cx(styles.btnMenuItem, styles.login)}
                            >
                                Вход
                            </button>

                            <div className={styles.overlayMenu}>
                                <ul className={styles.mobMenu}>
                                    <li>
                                        <a href={routes.pages.people}>Резюме</a>
                                    </li>
                                    <li>
                                        <a href={routes.pages.offers}>
                                            Вакансии
                                        </a>
                                    </li>
                                    <li>
                                        <a href={routes.pages.about}>
                                            О проекте
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </Fragment>
                    ) : (
                        <Fragment>
                            <a href="/" className={styles.logo} />
                            <ul className={styles.menu}>
                                <li>
                                    <a href={routes.pages.people}>Резюме</a>
                                </li>
                                <li>
                                    <a href={routes.pages.offers}>Вакансии</a>
                                </li>
                                <li>
                                    <a href={routes.pages.about}>О проекте</a>
                                </li>
                            </ul>
                        </Fragment>
                    )}
                </div>
                <div className={cx(styles.contentChild, styles.right)}>
                    <a
                        href={routes.pages.register}
                        className={cx(styles.btnMenuItem, styles.publish)}
                    >
                        Опубликовать вакансию
                    </a>
                    <button className={cx(styles.btnMenuItem, styles.register)}>
                        Регистрация
                    </button>
                    <button className={cx(styles.btnMenuItem, styles.login)}>
                        Вход
                    </button>
                </div>
                <div className={styles.mobile}>
                    <button
                        className={cx(styles.mobMenuBtn, {
                            [styles.mobMenuOpened]: this.state.isMobMenuOpen,
                        })}
                        onClick={this.onClickMobMenu}
                    />
                </div>
            </div>
        );
    }

    rendeHirer() {
        const { isMobMenuOpen, activeSubnav } = this.state;
        return (
            <div className={styles.content}>
                <div className={cx(styles.contentChild, styles.left)}>
                    {isMobMenuOpen ? (
                        <Fragment>
                            <button
                                className={cx(styles.btnMenuItem, styles.forms)}
                            >
                                0 анкет
                            </button>
                            <button
                                className={cx(
                                    styles.btnMenuItem,
                                    styles.wallet
                                )}
                            >
                                0 ₽
                            </button>
                            <button
                                className={cx(
                                    styles.btnMenuItem,
                                    styles.profile
                                )}
                            >
                                Профиль
                            </button>

                            <div className={styles.overlayMenu}>
                                <ul className={styles.mobMenu}>
                                    <li>
                                        <a href={routes.pages.people}>Резюме</a>
                                    </li>
                                    <li>
                                        <a href={routes.pages.offers}>
                                            Вакансии
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                            href={routes.pages.about}
                                            className={cx({
                                                [styles.isNewResponses]: true,
                                            })}
                                        >
                                            Отклики
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </Fragment>
                    ) : (
                        <Fragment>
                            <a href="/" className={styles.logo} />
                            <ul className={styles.menu}>
                                <li>
                                    <a
                                        href={routes.pages.people}
                                        onMouseEnter={this.onMouseEnter}
                                        data-target="linkNav"
                                        data-nav-id="1"
                                    >
                                        Резюме
                                    </a>

                                    <ul
                                        className={cx(styles.subMenu, {
                                            [styles.subMenuOpened]:
                                                activeSubnav === '1',
                                        })}
                                        data-target="subnav"
                                        data-id="1"
                                    >
                                        <li>
                                            <a href="#" data-target="linkNav">
                                                Поиск резюме
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-target="linkNav">
                                                Открытые
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-target="linkNav">
                                                Отобранные резюме
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a
                                        href={routes.pages.offers}
                                        onMouseEnter={this.onMouseEnter}
                                        data-target="linkNav"
                                        data-nav-id="2"
                                    >
                                        Вакансии
                                    </a>

                                    <ul
                                        className={cx(styles.subMenu, {
                                            [styles.subMenuOpened]:
                                                activeSubnav === '2',
                                        })}
                                        data-target="subnav"
                                        data-id="2"
                                    >
                                        <li>
                                            <a href="#" data-target="linkNav">
                                                Мои вакансии
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#" data-target="linkNav">
                                                Поиск вакансий
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a
                                        href={routes.pages.about}
                                        className={cx({
                                            [styles.isNewResponses]: true,
                                        })}
                                    >
                                        Отклики
                                    </a>
                                </li>
                            </ul>
                        </Fragment>
                    )}
                </div>
                <div className={cx(styles.contentChild, styles.right)}>
                    <ul className={styles.menu}>
                        <li>
                            <a
                                href={routes.pages.register}
                                className={cx(
                                    styles.btnMenuItem,
                                    styles.publish
                                )}
                            >
                                Опубликовать вакансию
                            </a>
                        </li>
                        <li>
                            <button
                                className={cx(styles.btnMenuItem, styles.forms)}
                            >
                                0 анкет
                            </button>
                        </li>
                        <li>
                            <button
                                className={cx(
                                    styles.btnMenuItem,
                                    styles.wallet
                                )}
                            >
                                0 ₽
                            </button>
                        </li>
                        <li>
                            <button
                                className={cx(
                                    styles.btnMenuItem,
                                    styles.profile
                                )}
                                onMouseEnter={this.onMouseEnter}
                                data-target="linkNav"
                                data-nav-id="3"
                            >
                                Профиль
                            </button>

                            <ul
                                className={cx(styles.subMenu, {
                                    [styles.subMenuOpened]:
                                        activeSubnav === '3',
                                })}
                                data-target="subnav"
                                data-id="3"
                            >
                                <li>
                                    <a href="#" data-target="linkNav">
                                        Настройки
                                    </a>
                                </li>
                                <li>
                                    <a href="#" data-target="linkNav">
                                        Выход
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

                <div className={styles.mobile}>
                    <button
                        className={cx(styles.mobMenuBtn, {
                            [styles.mobMenuOpened]: this.state.isMobMenuOpen,
                        })}
                        onClick={this.onClickMobMenu}
                    />
                </div>
            </div>
        );
    }

    renderClient() {
        const { isMobMenuOpen } = this.state;

        return (
            <div className={styles.content}>
                <div className={cx(styles.contentChild, styles.left)}>
                    {isMobMenuOpen ? (
                        <Fragment>
                            <button
                                className={cx(
                                    styles.btnMenuItem,
                                    styles.profile
                                )}
                            >
                                Моя анкета
                            </button>

                            <div className={styles.overlayMenu}>
                                <ul className={styles.mobMenu}>
                                    <li>
                                        <a href={routes.pages.people}>
                                            Вакансии
                                        </a>
                                    </li>
                                    <li>
                                        <a href={routes.pages.offers}>
                                            Моя лента вакансий
                                        </a>
                                    </li>
                                    <li>
                                        <a href={routes.pages.about}>
                                            Отобранные вакансии
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </Fragment>
                    ) : (
                        <Fragment>
                            <a href="/" className={styles.logo} />
                            <ul className={styles.menu}>
                                <li>
                                    <a href={routes.pages.people}>Вакансии</a>
                                </li>
                                <li>
                                    <a href={routes.pages.offers}>
                                        Моя лента вакансий
                                    </a>
                                </li>
                                <li>
                                    <a href={routes.pages.about}>
                                        Отобранные вакансии
                                    </a>
                                </li>
                            </ul>
                        </Fragment>
                    )}
                </div>
                <div className={cx(styles.contentChild, styles.right)}>
                    <button className={cx(styles.btnMenuItem, styles.profile)}>
                        Моя анкета
                    </button>
                </div>
                <div className={styles.mobile}>
                    <button
                        className={cx(styles.mobMenuBtn, {
                            [styles.mobMenuOpened]: this.state.isMobMenuOpen,
                        })}
                        onClick={this.onClickMobMenu}
                    />
                </div>
            </div>
        );
    }

    render() {
        return (
            <header className={styles.wrap}>
                {/* {this.renderNonAuth()} */}
                {this.rendeHirer()}
                {/* {this.renderClient()} */}
            </header>
        );
    }
}
