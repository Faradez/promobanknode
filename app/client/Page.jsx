import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Helmet from 'react-helmet';
import cx from 'classnames';

import styles from './app.css';

class Page extends Component {
    static propTypes = {
        main: PropTypes.oneOfType([
            PropTypes.arrayOf(PropTypes.node),
            PropTypes.node,
        ]),
        header: PropTypes.node,
        footer: PropTypes.node,
        pageClassName: PropTypes.string,
    };

    render() {
        const { main, header, footer, pageClassName } = this.props;

        return (
            <div className={cx(styles.page, pageClassName)}>
                <Helmet htmlAttributes={{ lang: 'ru' }} />

                {header && (
                    <header.type
                        {...header.props}
                    />
                )}

                <div id="page-body" className={cx('pageBody', styles.pageBody)}>
                    {main}
                </div>
                {footer}
            </div>
        );
    }
}

export default Page;
