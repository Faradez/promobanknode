/* eslint-disable */
const genericNames = require('generic-names');

module.exports = {
    extensions: ['.css'],
    // the custom template for the generic classes
    generateScopedName(name, filepath, css) {
        const generate = genericNames('[name]_[local]__[hash:base64:5]', {
            context: process.cwd(),
        });

        return generate(name, filepath.replace('compiled/client', 'app/client'));
    },
};

