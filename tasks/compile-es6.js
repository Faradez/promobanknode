/* eslint-disable import/no-extraneous-dependencies */
const gulp = require('gulp');
const babel = require('gulp-babel');

gulp.task('compile-es6', () =>
    gulp
        .src(['compiled/**/*.js', 'compiled/**/*.jsx'])
        .pipe(babel())
        .pipe(gulp.dest('compiled'))
);
