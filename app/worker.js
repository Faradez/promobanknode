// @flow
/* eslint no-param-reassign: [0] */
/* eslint global-require: [0] */
/* eslint import/first: [0] */

import express from 'express';
import slash from 'express-slash';
import serveStatic from 'serve-static';
import timer from 'contimer';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import csrf from 'csurf';


import 'css-modules-require-hook/preset';

import './lib/express-extensions';

import config from './lib/config';
import Logger from './lib/logger';
import registerErrorHandler from './lib/errorsLogger';

import RenderMiddleware from './middleware/RenderMiddleware';

const workerLogger = Logger('Worker');

if (config.IS_ENV_DEVELOPMENT || config.IS_ENV_TESTING) {
    workerLogger.info(config);

    registerErrorHandler();
}

const app = express();

app.disable('x-powered-by');
app.enable('strict routing');
app.enable('trust proxy');

app.use((req, res, next) => {
    res.locals.errors = {};

    req.state = {};

    timer.start(req, 'total');
    next();
});

app.use(serveStatic('./static/'));
app.use('/static/', serveStatic('./static/'));

app.use(cookieParser());
app.use(bodyParser.json());
app.use(csrf({ cookie: true }));

if (config.IS_SERVE_STATIC) {
    app.use(serveStatic('./'));

    app.use((req, res, next) => {
        res.header('Cache-Control', 'no-cache, no-store, must-revalidate');
        res.header('Pragma', 'no-cache');
        res.header('Expires', '0');
        next();
    });
}


// Create the router using the same routing options as the app.
const router = express.Router({
    caseSensitive: Boolean(app.get('case sensitive routing')),
    strict: Boolean(app.get('strict routing')),
});

app.use(router);
app.use(slash());

app.use(RenderMiddleware);

const listenId = process.env.USER === 'root' ? 80 : config.serverPort;

app.listen(listenId, () => {
    workerLogger.info(
        typeof process.env.NODE_ENV === 'string'
            ? `NODE_ENV=${process.env.NODE_ENV}`
            : 'Unknown NODE_ENV'
    );
    workerLogger.info(
        `external address: ${config.externalAddress}:${listenId}`
    );
    workerLogger.info(`listen port: ${listenId}`);
});
