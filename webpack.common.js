const path = require('path');
const AssetsPlugin = require('assets-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    entry: {
        vendor: ['babel-polyfill'],
    },
    output: {
        path: path.join(__dirname, 'dist'),
        pathinfo: true,
        chunkFilename: '[name].chunk.js',
        jsonpFunction: 'webpackMainJsonp',
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.(ttf|woff|eot)/,
                loader: 'file-loader?mimetype=application/octet-stream',
            },
        ],
    },
    stats: 'detailed',
    resolve: {
        extensions: ['.js', '.json', '.jsx'],
        modules: ['node_modules'],
    },
    resolveLoader: {
        extensions: ['.js', '.jsx'],
        modules: ['node_modules'],
    },
    plugins: [
        new AssetsPlugin({
            filename: 'assets.json',
        }),
        // eslint-disable-next-line
        new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /ru/),
        new webpack.IgnorePlugin(/^\.\/config$/, /\/app\/lib$/),
    ],
};
