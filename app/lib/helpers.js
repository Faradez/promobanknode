export const makeAutoresolvingPromiseFn = (func, valueIfUnresolved) => (
    ...params
) => Promise.resolve(func(...params)).catch(() => valueIfUnresolved);


export function getStartTime() {
    return process.hrtime();
}

export function getElapsedTime(startTime) {
    const elapsedTuple = process.hrtime(startTime);
    // prettier-ignore
    return Math.round((elapsedTuple[0] * 1000) + (elapsedTuple[1] / 1000000));
}
