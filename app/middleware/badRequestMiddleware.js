module.exports = function badRequestMiddleware() {
    return function badRequestMiddlewareHandler(req, res, next) {
        if (res.locals.errors && res.locals.errors.code === 400) {
            res.status(400).end();
            return;
        }

        next();
    };
};
