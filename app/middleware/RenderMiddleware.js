import Immutable from 'immutable';
import transit from 'transit-immutable-js';
import moment from 'moment';

import {
    getRenderOptionsFromCache,
    getRenderOptions,
    cacheRenderOptions,
    renderLayout,
} from '../lib/render';

moment.locale('ru');

const layout = 'default';

export default function RenderMiddleware(req, res, next) {
    const state = req.getState();

    const {
        originalUrl, headers, siteUser,
    } = req;

    const isAjax =
        headers.accept &&
        headers.accept.split(/\s*,\s*/).includes('application/json');

    if (isAjax) {
        res.json(state);
        return;
    }

    if (
        originalUrl.indexOf('/dist') >= 0 ||
        originalUrl.indexOf('/client') >= 0
    ) {
        next();
        return;
    }

    state.location = originalUrl;

    const userData = {
        ...state.userData,
        csrfToken: req.csrfToken(),
        siteUser: siteUser || null,
    };

    state.userData = null;

    getRenderOptionsFromCache(state)
        .catch(() => getRenderOptions(state))
        .then((options) => {
            if (
                (options.staticContext.status || 200) === 200 &&
                !options.retrievedFromCache
            ) {
                cacheRenderOptions(state, options);
            }

            const optionsWithUserData = {
                ...options,
                userData: encodeURIComponent(
                    transit.toJSON(Immutable.fromJS(userData))
                ),
            };

            res.status(optionsWithUserData.staticContext.status || 200);
            res.send(renderLayout(optionsWithUserData, layout));
        })
        .catch((error) => {
            next(error);
        });
}
