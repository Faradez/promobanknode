const set = require('lodash/set');
const LOG_LEVELS = require('../logLevels');

function getConfigFromEnv(env) {
    const nodeEnv = env.NODE_ENV;
    const isProduction = ['production'].includes(nodeEnv);

    const config = {
        IS_PRODUCTION: isProduction,
        IS_ENV_DEVELOPMENT: nodeEnv === 'development' || !nodeEnv,
        IS_SERVE_STATIC: nodeEnv === 'development' || nodeEnv === 'docker',
        LOG_LEVEL: env.LOG_LEVEL || LOG_LEVELS.info.name,
        IS_HTTPS: isProduction,
    };

    const BaseMapper = fn => input =>
        (typeof input !== 'string' ? null : fn(input));

    const StringMapper = BaseMapper(input => String(input));
    const IntMapper = BaseMapper(input => parseInt(input, 10));
    const BooleanMapper = BaseMapper(input => Boolean(input));
    // const FloatMapper = BaseMapper(input => parseFloat(input));

    [
        {
            envKey: 'BUILD_NUMBER',
            configKey: 'buildNumber',
            mapper: IntMapper,
        },
        {
            envKey: 'LOG_LEVEL',
            configKey: 'LOG_LEVEL',
            mapper: StringMapper,
        },
        {
            envKey: 'IS_STABLE',
            configKey: 'isStable',
            mapper: StringMapper,
        },
        {
            envKey: 'API_URL_V0',
            configKey: 'apiUrl.v0',
            mapper: StringMapper,
        },
        {
            envKey: 'API_URL_V1',
            configKey: 'apiUrl.v1',
            mapper: StringMapper,
        },
        {
            envKey: 'REDIS_API_HOST',
            configKey: 'redis.api.host',
            mapper: StringMapper,
        },
        {
            envKey: 'REDIS_API_PORT',
            configKey: 'redis.api.port',
            mapper: IntMapper,
        },
        {
            envKey: 'REDIS_API_DB',
            configKey: 'redis.api.db',
            mapper: IntMapper,
        },
        {
            envKey: 'REDIS_SESSION_PORT',
            configKey: 'redis.session.port',
            mapper: IntMapper,
        },
        {
            envKey: 'REDIS_SESSION_DB',
            configKey: 'redis.session.db',
            mapper: IntMapper,
        },
        {
            envKey: 'REDIS_RENDER_HOST',
            configKey: 'redis.render.host',
            mapper: StringMapper,
        },
        {
            envKey: 'REDIS_RENDER_PORT',
            configKey: 'redis.render.port',
            mapper: IntMapper,
        },
        {
            envKey: 'REDIS_RENDER_DB',
            configKey: 'redis.render.db',
            mapper: IntMapper,
        },
        {
            envKey: 'SERVER_NAME',
            configKey: 'serverName',
            mapper: StringMapper,
        },
        {
            envKey: 'IS_SERVE_STATIC',
            configKey: 'IS_SERVE_STATIC',
            mapper: BooleanMapper,
        },
        {
            envKey: 'PORT',
            configKey: 'serverPort',
            mapper: IntMapper,
        },
    ].forEach((value) => {
        const mappedValue = value.mapper(env[value.envKey]);

        if (mappedValue === null) {
            return;
        }

        set(config, value.configKey, mappedValue);
    });

    return config;
}

module.exports = getConfigFromEnv;
