const autoprefixer = require('autoprefixer');
const nested = require('postcss-nested');
const atImport = require('postcss-import');
const customProperties = require('postcss-custom-properties');
const colorAlpha = require('postcss-color-alpha');

module.exports = {
    plugins: [
        autoprefixer,
        atImport(),
        customProperties(),
        nested,
        colorAlpha(),
    ],
};

