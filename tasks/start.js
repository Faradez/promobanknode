/* eslint-disable import/no-extraneous-dependencies */

const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const defaultConfig = require('../nodemon.json');
const debugConfig = require('../nodemon-debug.json');

gulp.task('start', () => {
    const config = process.env.NODE_DEBUG ? debugConfig : defaultConfig;
    return nodemon(config);
});
