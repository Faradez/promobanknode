const merge = require('lodash/merge');
const getExternalAddress = require('./helpers/getExternalAddress');
const getConfigFromEnv = require('./helpers/getConfigFromEnv');
const setClientConfig = require('../client/lib/setClientConfig');
const getClientConfig = require('./helpers/getClientConfig');
const clientConfig = require('../client/config');
const settings = require('../settings');

const fallbackConfig = {
    redisApiPrefetchThreshold: 0.33,
    externalAddress: getExternalAddress(),
    IS_PRODUCTION: false,
    IS_ENV_DEVELOPMENT: true,
    IS_SERVE_STATIC: false,
    IS_HTTPS: false,
    buildNumber: 'emptyBuild',
};

const finalConfig = Object.freeze(
    merge(
        {},
        fallbackConfig,
        settings.default,
        settings[
            process.env.NODE_DOCKER_ENV || process.env.NODE_ENV || 'default'
        ],
        getConfigFromEnv(process.env)
    )
);

setClientConfig(clientConfig, getClientConfig(finalConfig));

module.exports = finalConfig;
