module.exports = {
    debug: { name: 'debug', level: 0, consoleMethod: 'info' },
    info: { name: 'info', level: 1, consoleMethod: 'info' },
    log: { name: 'log', level: 2, consoleMethod: 'log' },
    error: { name: 'error', level: 3, consoleMethod: 'error' },
};
