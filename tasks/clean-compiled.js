/* eslint-disable import/no-extraneous-dependencies */

const gulp = require('gulp');
const clean = require('gulp-clean');

gulp.task('clean-compiled', () => gulp.src('compiled/', { read: false }).pipe(clean()));
