/* eslint global-require: [0] */
/* eslint import/no-dynamic-require: [0] */

require('babel-core');
require('babel-register')();

const gulp = require('gulp');
const runSequence = require('run-sequence');
const fs = require('fs');

fs.readdirSync('./tasks').forEach(item => require(`./tasks/${item}`));

gulp.task('default', () => {
    runSequence('compile-layouts', ['start']);
});

gulp.task('build', () => {
    runSequence(
        'clean-compiled',
        'compile-layouts',
        'move-files-for-node',
        'compile-es6'
    );
});
