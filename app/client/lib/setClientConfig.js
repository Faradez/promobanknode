const setClientConfig = (config, clientConfig) => {
    Object.assign(config, clientConfig);
};

module.exports = setClientConfig;
