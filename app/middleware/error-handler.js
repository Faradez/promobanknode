import util from 'util';

import config from '../lib/config';
import Logger from '../lib/logger';

const logger = Logger('ErrorHandler');

export default function ErrorHandler(err, req, res, next) {
    if (err && err.code === 'EBADCSRFTOKEN') {
        res.status(403);
        res.send('Bad CSRF token');
        return;
    }

    const state = req.getState();
    const isSimpleError = util.isError(err);

    const statusCode =
        state && state.serverError && state.serverError.statusCode
            ? state.serverError.statusCode
            : 500;

    if (statusCode >= 400) {
        res.status(statusCode);
    }

    if (statusCode >= 500) {
        if (isSimpleError) {
            logger.error(
                `error: ${err.stack}, originalUrl: ${req.originalUrl}`
            );
        } else {
            logger.error(
                `error: ${JSON.stringify(err)}, originalUrl: ${req.originalUrl}`
            );
        }
    }

    if (req.xhr) {
        if (isSimpleError) {
            if (!config.IS_PRODUCTION) {
                res.send(err.stack);
            }

            res.end();
            return;
        }
    }

    if (statusCode >= 400) {
        res.end();
        return;
    }

    next();
}
