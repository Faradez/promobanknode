import util from 'util';
import moment from 'moment';
import chalk from 'chalk';

import config from './config';
import LOG_LEVELS from './logLevels';

const colors = new chalk.constructor({
    enabled: config.IS_ENV_DEVELOPMENT,
});

const NAMESPACE_PREFIX = 'pb:';

const currentLogLevel = LOG_LEVELS[config.LOG_LEVEL].level;

function logger() {}

/* eslint-disable no-console */
Object.keys(LOG_LEVELS).forEach((key) => {
    const logOption = LOG_LEVELS[key];

    // eslint-disable-next-line func-names
    logger.prototype[logOption.name] = function (...args) {
        if (logOption.level < currentLogLevel) {
            return;
        }

        let nameColor = colors.yellow;

        if (logOption.level >= 3) {
            nameColor = colors.bold.red;
        }

        const dateTime = colors.inverse(
            moment().format('YYYY-MM-DD HH:mm:ss.SSS')
        );

        const pid = `pid:${process.pid}`;
        const formattedArgs = util.format(...args);
        const lineEnding = config.IS_ENV_DEVELOPMENT ? '\n' : '';

        const line = util.format(
            '%s | %s | %s | %s | %s',
            dateTime,
            nameColor(logOption.name),
            this.namespace,
            pid,
            formattedArgs
        );

        process.nextTick(() => {
            console[logOption.consoleMethod](`${line}${lineEnding}`);
        });
    };
});

/* eslint-enable no-console */

function createLogger(namespace) {
    return Object.create(logger.prototype, {
        namespace: {
            value: namespace
                ? NAMESPACE_PREFIX + namespace.toLowerCase()
                : 'default',
        },
    });
}

export default createLogger;
