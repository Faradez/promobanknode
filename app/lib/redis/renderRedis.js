import createClient from './createClient';
import config from '../config';

const redisConfig = config.redis.render;

const client = createClient({
    host: redisConfig.host,
    port: redisConfig.port,
    db: redisConfig.db,
    name: 'render',
});

export default {
    /**
     *
     * @param {String} key
     * @param {String} body
     * @param {Number} expire
     */
    set(key, body, expire) {
        if (!client.connected) {
            return;
        }

        if (typeof expire !== 'undefined' && expire <= 0) {
            return;
        }

        if (expire) {
            client.setex(key, expire, body);
        } else {
            client.set(key, body);
        }
    },

    /**
     *
     * @param {String} key
     * @returns {*|promise}
     */
    get(key) {
        if (!client.connected) {
            return Promise.reject();
        }

        return new Promise((resolve, reject) => {
            client.get(key, (err, reply) => {
                if (err) {
                    reject();
                    return;
                }

                if (reply) {
                    resolve(reply);
                } else {
                    reject();
                }
            });
        });
    },
};
