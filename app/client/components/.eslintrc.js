module.exports = {
    rules: {
        complexity: ['error', 4],
        curly: ['error', 'all'],
    },
};
