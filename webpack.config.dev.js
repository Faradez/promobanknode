const webpack = require('webpack');
const path = require('path');
const merge = require('webpack-merge');

const configApp = require('./app/lib/config');
const settings = require('./app/settings');
const commonConfig = require('./webpack.common');

const PORT = settings.default.webpackDevServerPort;
const publicPath = `http://${configApp.externalAddress}:${PORT}/dist/`;

module.exports = merge.smart(commonConfig, {
    entry: {
        app: [
            'react-hot-loader/patch',
            `webpack-dev-server/client?http://${configApp.externalAddress}:${PORT}`,
            'webpack/hot/only-dev-server',
            path.join(__dirname, 'app/client/client.jsx'),
        ],
    },

    output: {
        filename: '[name].js',
        publicPath,
    },

    devServer: {
        hot: true,
        // enable HMR on the server

        contentBase: path.resolve(__dirname, 'dist'),
        // match the output path

        publicPath,
        // match the output `publicPath`

        host: configApp.externalAddress,
        port: PORT,

        inline: true,

        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'X-Requested-With',
        },
    },

    devtool: 'inline-source-map',

    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    plugins: ['react-hot-loader/babel'],
                },
            },
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'img/[name].[hash:6].[ext]',
                            publicPath,
                        },
                    },
                    {
                        loader: 'svgo-loader',
                        options: {
                            plugins: [
                                { removeTitle: true },
                                { convertColors: { shorthex: false } },
                                { convertPathData: false },
                            ],
                        },
                    },
                ],
            },
            {
                test: /vendor\.css$/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: name =>
                    name.endsWith('.css') && !name.endsWith('vendor.css'),
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: '[name]_[local]__[hash:base64:5]',
                        },
                    },
                    'postcss-loader',
                ],
            },
            {
                test: /\.(png|gif|jpe?g)/,
                loader: 'file-loader',
                options: {
                    name: 'img/[name].[hash:6].[ext]',
                    publicPath,
                },
            },
        ],
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: module => /node_modules/.test(module.resource),
        }),
        new webpack.EnvironmentPlugin({
            NODE_ENV: 'development',
            DEBUG: true,
        }),
    ],
});
