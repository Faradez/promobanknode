import redis from 'redis';

import config from '../config';
import Logger from '../logger';

export default function createClient({
    host, port, db, name = 'default',
}) {
    const logger = Logger(`redis/redisInstance:${name}`);
    let client;

    const opts = {
        host,
        port,
        db,
        retry_strategy: (options) => {
            client.connected = false;
            logger.debug('set "CONNECTED=false"');
            logger.debug('connect attempt count', options.attempt);
            if (options.error) {
                logger.error('connect opt.error:', options.error);
            }
            return Math.min(options.attempt * 100, 10000);
        },
        enable_offline_queue: false,
    };

    if (config.IS_ENV_DEVELOPMENT) {
        opts.retry_strategy = () => null;
    }

    client = redis.createClient(opts);
    client.connected = false;

    client.on('end', () => {
        logger.info('lose connection to redis server');
        logger.debug('set "CONNECTED=false"');
        client.connected = false;
    });

    client.on('reconnecting', () => {
        logger.info('try reconnect to redis server');
    });

    client.on('error', (err) => {
        logger.info('some error happened', err.message || 'unknown error');
        logger.error(err);
        if (err && err.code === 'ECONNREFUSED') {
            logger.debug('set "CONNECTED=false"');
            client.connected = false;
        }
    });

    client.on('ready', () => {
        logger.info('client ready');

        client.select(db, () => {
            client.connected = true;
            logger.debug('set "CONNECTED=true"');
            logger.info(`select db ${db}`);
        });
    });

    client.on('connect', () => {
        logger.info(`connected, url: ${host}, port: ${port}`);
    });

    return client;
}
