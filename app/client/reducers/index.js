import Immutable from 'immutable';

const reducers = [
];

export default function RootReducer(state = Immutable.Map(), action) {
    let newState = state;
    reducers.forEach((reducer) => {
        if (typeof reducer[action.type] === 'function') {
            newState = reducer[action.type](newState, action);
        }
    });

    if (
        newState.get('isDevelopment') ||
        newState.get('isTesting') ||
        window.isDebug
    ) {
        window.state = newState;
    }

    return newState;
}
