import throttle from 'lodash/throttle';

export default function throttleHandler(...args) {
    const thrl = throttle(...args);
    return function throttleHandlerClosure(e, ...a) {
        e.persist();
        return thrl(e, ...a);
    };
}
