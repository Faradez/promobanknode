export const SECOND = 1;
export const MIN = 60 * SECOND;
export const HOUR = MIN * 60;
export const DAY = HOUR * 24;
