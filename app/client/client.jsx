/* eslint no-plusplus: [0] */

import React from 'react';
import ReactDom from 'react-dom';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import { createStore, applyMiddleware, compose } from 'redux';
import reduceReducers from 'reduce-reducers';
import { enableBatching } from 'redux-batched-actions';
import {
    ConnectedRouter,
    routerReducer,
    routerMiddleware,
} from 'react-router-redux';
import createHistory from 'history/createBrowserHistory';
import Immutable from 'immutable';
import transit from 'transit-immutable-js';
import moment from 'moment';

import './settingClientConfig';
import './shims';

import './vendor.css'; // this import must be upper than other import includes styles
import Routes from './Routes';

import reducers from './reducers';
import { setCSRF } from './lib/api';

moment.locale('ru');
window.moment = moment;

function render({ store, children }) {
    ReactDom.hydrate(
        <AppContainer>
            <Provider store={store}>{children}</Provider>
        </AppContainer>,
        document.getElementById('app-container')
    );
}

const history = createHistory();
const routerWithHistory = routerMiddleware(history);

document.addEventListener('DOMContentLoaded', () => {
    const initialState = transit
        .fromJSON(window.transitState)
        .set('routing', Immutable.Map());

    window.transitState = null;

    const userData = transit.fromJSON(window.transitUserData);
    window.transitUserData = null;

    const batchedReducers = enableBatching(reducers);

    const routerReducerWrapper = (state = Immutable.Map(), action) => {
        const reducedState = routerReducer(state.get('router'), action);
        return state.set('router', reducedState);
    };

    const rootReducer = reduceReducers(batchedReducers, routerReducerWrapper);

    /* eslint-disable no-underscore-dangle */
    const composeEnhancers =
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const store = createStore(
        rootReducer,
        initialState,
        composeEnhancers(applyMiddleware(routerWithHistory, thunk))
    );
    /* eslint-enable */

    const state = store.getState();

    setCSRF(userData.get('csrfToken'));

    render({
        store,
        state,
        userData,
        children: <ConnectedRouter history={history}>{Routes}</ConnectedRouter>,
    });

    if (module.hot) {
        module.hot.accept('./Routes.jsx', () => {
            /* eslint-disable global-require  */
            const RoutesComponent = require('./Routes').default;
            /* eslint-enable */

            render({
                store,
                state,
                userData,
                children: (
                    <ConnectedRouter history={history}>
                        {RoutesComponent}
                    </ConnectedRouter>
                ),
            });
        });
    }
});
