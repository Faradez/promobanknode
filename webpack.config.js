const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
// const BrotliPlugin = require('brotli-webpack-plugin');
const merge = require('webpack-merge');
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const buildNumber = process.env.BUILD_NUMBER;

if (!buildNumber) {
    throw new Error('Unknown buildNumber');
}

const publicPath = '/dist/';

const commonConfig = require('./webpack.common');

module.exports = merge(commonConfig, {
    entry: {
        app: path.join(__dirname, 'app/client/client.jsx'),
    },
    output: {
        filename: '[name].[chunkhash:6].js',
        publicPath,
    },
    module: {
        rules: [
            {
                test: /\.svg$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'img/[name].[hash:6].[ext]',
                            publicPath,
                        },
                    },
                    {
                        loader: 'svgo-loader',
                        options: {
                            plugins: [
                                { removeTitle: true },
                                { convertColors: { shorthex: false } },
                                { convertPathData: false },
                            ],
                        },
                    },
                ],
            },
            {
                test: /vendor\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true,
                            },
                        },
                    ],
                }),
            },
            {
                test: name =>
                    name.endsWith('.css') && !name.endsWith('vendor.css'),
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                minimize: true,
                                importLoaders: 1,
                                localIdentName:
                                    '[name]_[local]__[hash:base64:5]',
                            },
                        },
                        {
                            loader: 'postcss-loader',
                        },
                    ],
                }),
            },
            {
                test: /\.(png|gif|jpe?g)/,
                loader: 'file-loader',
                options: {
                    name: 'img/[name].[hash:6].[ext]',
                    publicPath,
                },
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(['dist'], {
            root: path.resolve(__dirname),
            verbose: true,
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: module => /node_modules/.test(module.resource),
        }),
        new webpack.EnvironmentPlugin({
            DEBUG: false,
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production'),
        }),
        new ExtractTextPlugin({
            filename: 'app.[chunkhash:6].css',
            allChunks: true,
        }),
        new UglifyJSPlugin({
            cache: true,
            parallel: true,
            sourceMap: true,
            uglifyOptions: {
                mangle: false,
                warnings: false,
                output: {
                    comments: false,
                },
                compress: {
                    keep_fnames: true,
                },
            },
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        // new BrotliPlugin({
        //     asset: '[path].br[query]',
        //     test: /\.(js|css|svg|ttf|eot)$/,
        //     quality: 11,
        //     lgwin: 22,
        //     minRatio: 0.8,
        // }),
        // new BundleAnalyzerPlugin(),
    ],
});
