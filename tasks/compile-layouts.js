/* eslint-disable no-new-func */

const gulp = require('gulp');
const through = require('through2');
const fs = require('fs');

const layouts = {};

function bufferContents(file, enc, cb) {
    const layout = file.relative.replace(/\.html$/, '');
    const html = file.contents.toString();

    const chunks = html.split(/(\{\{\s*[\w-.]+\s*\}\})/).map((chunk) => {
        const m = chunk.match(/\{\{\s*([\w-.]+)\s*\}\}/);

        if (m) {
            return `data['${m[1]}']`;
        }

        const chunkOut = chunk.replace(/'/g, "\\'").replace(/\n+/g, '\\n');

        return `'${chunkOut}'`;
    });

    layouts[layout] = new Function(
        'data',
        `return [${chunks.join(',')}].join('');`
    ).toString();

    return cb();
}

function writeCompiled(cb) {
    const out = [
        'export default {',
        ...Object.keys(layouts).map(
            layout => `'${layout}': ${layouts[layout]},`
        ),
        '};',
    ];

    fs.writeFile('app/layouts/index.js', out.join('\n'), (err) => {
        if (err) {
            console.error(err);
        }
        cb();
    });
}

gulp.task('compile-layouts', () =>
    gulp
        .src('app/layouts/*.html')
        .pipe(through.obj(bufferContents, writeCompiled))
);
