
const settings = {
    default: {
        serverPort: 3000,
        webpackDevServerPort: 3333,
        staticHost: '',
        apiUrl: {
            v0: '/',
        },
        serverName: 'promobank.local',
        apiKey: 'ssssecret-key',
        redis: {
            api: {
                host: '127.0.0.1',
                port: 6379,
                db: 0,
            },
            session: {
                host: '127.0.0.1',
                port: 6379,
                db: 1,
            },
            render: {
                host: '127.0.0.1',
                port: 6379,
                db: 2,
            },
        },
    },
    production: {
        apiUrl: {
            v0: '/',
        },
        serverName: 'promobank.ru',
        redis: {
            api: {
                host: 'promobank.ru',
                port: 6381,
                db: 0,
            },
            session: {
                host: 'promobank.ru',
                port: 6381,
                db: 0,
            },
            render: {
                host: 'promobank.ru',
                port: 6381,
                db: 0,
            },
        },
    },

};

module.exports = settings;
