import setClientConfig from './lib/setClientConfig';
import config from './config';

setClientConfig(config, window.clientConfig);
