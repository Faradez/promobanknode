const Handlers = {
    main(req, res, next) {
        const { state } = req;

        state.page.type = 'main';

        next();
    },
};

export default function MainController(router) {
    router.get('/', Handlers.main);
}
